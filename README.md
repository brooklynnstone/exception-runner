Tasks to answer in your own README.md that you submit on Canvas:

1.. See logger.log, why is it different from the log to console? 
* The logger.log is only recording a set few logs within the program, while the console prints the error messages of uncaught and unhandled exceptions caused by logic errors.

2.. Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
* This line results from successful tests being run. The first and third tests were successful and threw no exceptions, 
so the logger printed this line.

3.. What does Assertions.assertThrows do?
* This method takes a Type and an "Executable" to look in. It then checks whether the given executable code threw a certain exception type.

4.. See TimerException and there are 3 questions
    
    1.  What is serialVersionUID and why do we need it? (please read on Internet)
        - It is a specific Long value which is used during deserialization to
        ensure that whoever sends and recieves the object have classes that are
        compatible with respect to serialization. If we don't declare it explicitly,
        it will be generated automatically, but due to the way different systems work, 
        it could result in a different serialVersionUID, so it's good to explicitly
        declare this value.
    2.  Why do we need to override constructors?
        - When a constructor needs to do something it originally would not have done
        or if the default action of the constructor is undesired. In this case, we
        override the constructor with two different actions. One initializes the message
        and the other initializes both the message and cause.
    3.  Why we did not override other Exception methods?
        - Since we are only needing to set the message and cause, it is not necessary
        to override any other methods, as we won't be needing or using them. This also
        ensures that the information we will be using as part of the exception is being
        saved to the correct members and is being used how we want it to be.	

5.. The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
* A static block is executed only once and is executed when an object of the class is created
or when a static member of the class is accessed. For this class, the static block creates an input
stream from the logger.properties file and is used for Configurations for the log manager.

6.. What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
* .md indicates that the README file is using the Markdown language. This is one way bitbucket formats text for
README files and any commits. This extension also allows a few HTML tags to be used in bitbucket.

7.. Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
* The main issue with this class was in the try, catch, finally blocks. timeNow is initialized
to null and only changed if timeToWait is not negative. So, if timeToWait is negative, timeNow is 
always null and gets used in the finally block which results in a NullPointerException. By checking
for null before calculating the time difference, the NullPointerException can be avoided.

8.. What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
* A TimerException is thrown but once it is, the finally block still uses the null value which causes an unchecked
exception, a NullPointerException. Since this in not dealt with, this means there's a logic error by the programmer.

9.. Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
* Done!

10.. Make a printScreen of your eclipse Maven test run, with console
* Done!

11.. What category of Exceptions is TimerException and what is NullPointerException
* TimerException is a checked exception and NullPointerException is unchecked.

12.. Push the updated/fixed source code to your own repository.
* Done!